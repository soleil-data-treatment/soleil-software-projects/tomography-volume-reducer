CC := gcc

CFLAGS := -O2 -Wall

TEST_VOL_FILE := example/raw.vol
TEST_VOLQ_FILE := example/reduced.vol

RM := rm -f
RMDIR := rm -r

QUIET = @


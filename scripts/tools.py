#!/usr/bin/env python
# -*- coding: utf-8 -*-


def get_indices(index_str):
    """
    Return a list of indices based on a string
    Exemple: '1-4,6,8-10' -> [1,2,3,4,6,8,9,10]
    """
    indices = []
    try:
        index_str_list = index_str.split(',')
    except AttributeError:
        return indices
    for s in index_str_list:
        if '-' in s:
            ind_min, ind_max = [int(x) for x in s.split('-')]
            inds = list(range(ind_min, ind_max+1))
            indices.extend(inds)
        else:
            indices.append(int(s))
    return indices


if __name__ == '__main__':
    index_str = '1-5,10,15-20,30,40'
    results = get_indices(index_str)
    print(results)

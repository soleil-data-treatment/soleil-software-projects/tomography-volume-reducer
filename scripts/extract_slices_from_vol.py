#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
import numpy as np
import struct

from tools import get_indices

IMAGE_SHAPE = (2048, 2048)
SLICE_NO = 0

LITTLE_ENDIAN_CODE = '<'
BIG_ENDIAN_CODE = '>'
ENDIAN_CODE_CHOICES = [LITTLE_ENDIAN_CODE, BIG_ENDIAN_CODE]

UINT8_CODE = 'B'
UINT16_CODE = 'H'
FLOAT32_CODE = 'f'
DTYPE_CODE_CHOICES = [UINT8_CODE, UINT16_CODE, FLOAT32_CODE]

DTYPE = LITTLE_ENDIAN_CODE + FLOAT32_CODE


def get_chunk_len(shape):
    """Return the chunk length based on the image dimension"""
    return shape[0] * shape[1]


def get_image(fin, image_shape=IMAGE_SHAPE, slice_no=SLICE_NO, dtype=DTYPE):
    """Given a file descriptor, extract an image slice"""
    count = get_chunk_len(image_shape)
    chunk_size = count * struct.calcsize(dtype)  # in bytes
    offset = (slice_no - 1) * chunk_size  # in bytes
    #  offset in np.fromfile is from the file’s current position => need to reset it to the beginning of the file
    fin.seek(0)
    image = np.fromfile(fin, dtype=dtype, count=count, offset=offset).reshape(image_shape)
    return image


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=str, help="Input binary file")
    parser.add_argument('-s', '--image-shape', type=int, nargs='+', default=IMAGE_SHAPE, help="Dimension of the the image [e.g., 2048 2048]")
    parser.add_argument('-i', '--slice-ids-str', type=str, default='0', help="List of wanted slice ids [e.g., '0-3,4,5-8']")
    parser.add_argument('-d', '--dtype-code', type=str, choices=DTYPE_CODE_CHOICES , default=FLOAT32_CODE, help="Data-type character code")
    parser.add_argument('-e', '--endianess-code',  type=str, choices=ENDIAN_CODE_CHOICES, default=LITTLE_ENDIAN_CODE, help="Endianess")
    parser.add_argument('-o', '--outfile', type=str, default='toto.vol', help="Name of the ouput file containing the wanted slices")
    args = parser.parse_args()

    slice_nos = get_indices(args.slice_ids_str)
    dtype = args.endianess_code + args.dtype_code  # e.g.,'<f'

    with open(args.infile, 'rb') as fin:
        result = np.array([], dtype=dtype)
        for slice_no in slice_nos:
            image = get_image(fin, image_shape=args.image_shape, slice_no=slice_no, dtype=dtype)
            flat_image = image.flatten()
            result = np.append(result, flat_image)
        
    with open(args.outfile, 'wb') as fout:
        result.tofile(fout, format=dtype)
        


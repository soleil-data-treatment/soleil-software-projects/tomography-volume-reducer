#pragma once

#include "common.h"

bool const utils_file_exists(char const*);
off_t const utils_get_file_size(char const*);
uint64_t const utils_get_file_size2(FILE*);
size_t utils_get_nb_chunks(size_t const, size_t const);
size_t utils_get_nb_bins(size_t const, size_t const);
custom_dtype_t utils_get_mean(custom_dtype_t*, size_t);
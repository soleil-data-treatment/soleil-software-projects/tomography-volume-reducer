#pragma once

#include "common.h"
#include "config.h"
#include "constants.h"
#include "option.h"
#include "utils.h"
#include "chunk.h"

int reducer_run(option);

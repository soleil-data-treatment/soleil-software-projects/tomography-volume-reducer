#include "option.h"

int option_usage() {
    char help_str[PATH_MAX];
    fprintf(stderr, "Usage: ./main.exe INFILE\n");
    sprintf(help_str, " [ -x NX ] -- Volume width [default: %d]\n", CONFIG_NX);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -y NY ] -- Volume height [default: %d]\n", CONFIG_NY);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -z NZ ] -- Volume depth [default: %d]\n", CONFIG_NZ);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -b BLOCK_SIZE ] -- Block size in all dimensions [default: %d]\n", CONFIG_BLOCK_SIZE);
    fprintf(stderr, help_str);
    sprintf(help_str, " [ -o OUTFILE ] -- Output file [default: %s]\n", CONFIG_OUTFILE);
    fprintf(stderr, help_str);
    exit(0);
}

option option_get(int argc, char* argv[argc+1]) {
    /* Default option values */
    option o = {
        .nx = CONFIG_NX,
        .ny = CONFIG_NY,
        .nz = CONFIG_NZ,
        .block_size_x = CONFIG_BLOCK_SIZE,
        .block_size_y = CONFIG_BLOCK_SIZE,
        .block_size_z = CONFIG_BLOCK_SIZE,
        .outfile = CONFIG_OUTFILE,
    };

    int index, opt;
    size_t block_size;
    while ((opt = getopt(argc, argv, "b:ho:x:y:z:")) != -1)
        switch (opt) {
            case 'b':
                sscanf(optarg, "%ld", &block_size);
                o.block_size_x = block_size;
                o.block_size_y = block_size;
                o.block_size_z = block_size;
                break;
            case 'h': 
                option_usage();
                exit(0);
            case 'o': 
                strcpy(o.outfile, optarg);
                break;
            case 'x':
                sscanf(optarg, "%ld", &o.nx);
                break;
            case 'y':
                sscanf(optarg, "%ld", &o.ny);
                break;
            case 'z':
                sscanf(optarg, "%ld", &o.nz);
                break;
            case '?':
                if (optopt == 'b')
                    fprintf(stderr, "Option -b requires an argument.\n");
                else if (optopt == 'x' || optopt == 'y' || optopt == 'z')
                    fprintf(stderr, "Option -x, -y and/or -z requires an argument.\n");
                else if (optopt == 'o')
                    fprintf(stderr, "Option -o requires an argument.\n");
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
                exit(EXIT_FAILURE);
            default: 
                abort();
        }

    debug_print("nx = %zu\n", o.nx);
    debug_print("ny = %zu\n", o.ny);
    debug_print("nz = %zu\n", o.nz);
    debug_print("block_size_x = %zu\n", o.block_size_x);
    debug_print("block_size_y = %zu\n", o.block_size_y);
    debug_print("block_size_z = %zu\n", o.block_size_z);
    debug_print("outfile = %s\n", o.outfile);

    debug_print("# args = %d, optind = %d\n", argc, optind);
    for (index = optind; index < argc; ++index) {
        debug_print("Non-option argument %s\n", argv[index]);
    }

    if ((argc - optind) != 1) {
        option_usage();
    }

    assert(o.block_size_x > 0);
    assert(o.block_size_y > 0);
    assert(o.block_size_z > 0);
    assert(o.nx > 0);
    assert(o.ny > 0);
    assert(o.nz > 0);

    char* infile = argv[optind];
    if (!infile || !utils_file_exists(infile))
        exit(EXIT_FAILURE);

    o.infile = infile;
    debug_print("Input file = %s\n", o.infile);

    return o;
}

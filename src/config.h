#pragma once

#include "common.h"

#define CONFIG_NX 2048
#define CONFIG_NY 2048
#define CONFIG_NZ 8
#define CONFIG_BLOCK_SIZE 2
#define CONFIG_OUTFILE "toto.vol"

#pragma once

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef FLAG_OPENMP
#include <omp.h>
#endif

#ifndef MIN
#define MIN(x,y) ((x)<(y)?(x):(y))
#endif
#ifndef MAX
#define MAX(x,y) ((x)>(y)?(x):(y))
#endif

#define COUNT(x) (sizeof(x)/sizeof(x[0]))

#define STRINGIFY(X) #X
#define STRGY(X) STRINGIFY(X)

#ifdef DEBUG
#define TRACE_ON 1
#else
#define TRACE_ON 0
#endif

#define debug_print(F, ...)                                                                             \
    do {                                                                                                \
        if (TRACE_ON)                                                                                   \
            fprintf(stderr, "DEBUG -- %s, %s:" STRGY(__LINE__) ": " F, __FILE__, __func__, __VA_ARGS__); \
    } while (false)

// Use alias for input data type
typedef float custom_dtype_t;

void* safe_calloc(size_t, size_t);
FILE* safe_fopen(const char*, const char*);

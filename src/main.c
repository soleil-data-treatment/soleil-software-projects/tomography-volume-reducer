#include "reducer.h"

int main(int argc, char* argv[argc+1]) {

    option o = option_get(argc, argv);

    int ret;
    ret = reducer_run(o);

    return ret;
}

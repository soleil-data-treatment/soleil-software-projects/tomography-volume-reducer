#pragma once

#include "common.h"
#include "config.h"
#include "utils.h"

typedef struct option option;

struct option {
    /* Mandatory */
    char* infile;
    /* Optional */
    size_t nx;  // width
    size_t ny;  // height
    size_t nz;  // depth
    size_t block_size_x;
    size_t block_size_y;
    size_t block_size_z;
    char outfile[PATH_MAX];
};

int option_usage();
option option_get(int, char**);


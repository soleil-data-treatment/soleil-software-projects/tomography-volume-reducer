#pragma once

#include "common.h"
#include "option.h"

void chunk_reduce(size_t, custom_dtype_t const*restrict, option o, size_t, size_t, custom_dtype_t*restrict out);

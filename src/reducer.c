#include "reducer.h"

int reducer_run(option o) {

    debug_print("Reducing the volume using a block size of (%zu, %zu, %zu)\n", o.block_size_x, o.block_size_y, o.block_size_z);

    // A chunk will be a sub-volume (cz slices of dimension nx * ny)
    size_t slice_len = o.nx * o.ny;
    debug_print("slice length = %zu\n", slice_len);
    size_t chunk_len = slice_len * o.block_size_z;
    debug_print("chunk length = %zu\n", chunk_len);
    uint8_t item_size = sizeof(custom_dtype_t);
    debug_print("item size (bytes) = %u\n", item_size);

    custom_dtype_t* data = safe_calloc(chunk_len, item_size);

    FILE* fin = safe_fopen(o.infile, "rb");

    size_t const infile_size = utils_get_file_size(o.infile);

    size_t const chunk_size = chunk_len * item_size;
    debug_print("chunk size (bytes) = %zu\n", chunk_size);

    size_t const nb_chunks = utils_get_nb_chunks(infile_size, chunk_size);

    FILE* fout = safe_fopen(o.outfile, "wb");

    // The output chunk will be a slice of dimension nx_out * ny_out
    size_t nx_out = utils_get_nb_bins(o.nx, o.block_size_x);
    size_t ny_out = utils_get_nb_bins(o.ny, o.block_size_y);
    size_t chunk_len_out = nx_out * ny_out;
    debug_print("chunk length in output = %zu\n", chunk_len_out);

    size_t const chunk_size_out = chunk_len_out * item_size;
    debug_print("chunk size in output (bytes) = %zu\n", chunk_size_out);

    custom_dtype_t* data_out = safe_calloc(chunk_len_out, item_size);

    for (size_t h = 0; h < nb_chunks; ++h) {

        int read_size = fread(data, 1, chunk_size, fin);
        debug_print("read_size (bytes) = %d\n", read_size);

        chunk_reduce(chunk_len, data, o, nx_out, chunk_len_out, data_out);

        fwrite(data_out, 1, chunk_size_out, fout);
    }

    free(data);
    free(data_out);

    fclose(fin);
    fclose(fout);

    return EXIT_SUCCESS;
}
#include "chunk.h"

void chunk_reduce(size_t len, custom_dtype_t const*restrict chunk, option o, size_t nx_out, size_t len_out, custom_dtype_t*restrict reduced_chunk) {

    // Store the block values in an temporary array
    size_t nb_items_in_bins = o.block_size_x * o.block_size_y * o.block_size_z;
    custom_dtype_t values[nb_items_in_bins];

    for (size_t index_out = 0; index_out < len_out; index_out++) {

        size_t i_out = index_out % nx_out;
        size_t j_out = index_out / nx_out;

        size_t i_start = o.block_size_y * i_out;
        size_t j_start = o.block_size_x * j_out;

        for (size_t k = 0; k < o.block_size_z; ++k) {
            for (size_t j = j_start; j < j_start + o.block_size_x; ++j) {
                for (size_t i = i_start; i < i_start + o.block_size_y; ++i) {
                    size_t index = i + o.nx * j + o.nx * o.ny * k;
                    size_t block_index = (i - i_start) + o.block_size_x * (j - j_start) + o.block_size_x * o.block_size_y * k;
                    values[block_index] = chunk[index];
                }
            }
        }

        custom_dtype_t value_in_block = utils_get_mean(values, nb_items_in_bins);
        reduced_chunk[index_out] = value_in_block;
    }
}
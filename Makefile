CONFIG ?= config.mk
include $(CONFIG)

LDLIBS := -lm

SOURCE := main.c
EXEC := $(SOURCE:.c=.exe)

SOURCES := $(wildcard src/*.c)
OBJECTS := $(SOURCES:.c=.o)

.PHONY: all debug check clean distclean

all: $(EXEC)

$(EXEC): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS) $(LDLIBS)

src/%.o: src/%.c
	$(CC) $(CFLAGS) -c $^ -o $@ $(LDFLAGS) $(LDLIBS)

debug: CFLAGS = -O0 -Wall
debug: CFLAGS += -g -DDEBUG
debug: CFLAGS += -pg
debug: $(EXEC)

openmp: CFLAGS += -DFLAG_OPENMP
openmp: CFLAGS += -fopenmp
openmp: $(EXEC)

openmp-debug: CFLAGS = -O0 -Wall
openmp-debug: CFLAGS += -g -DDEBUG
openmp-debug: openmp

check:
	$(QUIET) echo using $(CONFIG)
	$(QUIET) echo rebinning the raw volume
	./$(EXEC) -x 2048 -y 2048 -z 8 -b 2 -o $(TEST_VOLQ_FILE) $(TEST_VOL_FILE)

clean:
	$(RM) $(OBJECTS)
	$(RM) $(TEST_VOLQ_FILE) $(TEST_VOLU_FILE)
	$(RM) gmon.out

distclean: clean
	$(RM) *.exe
